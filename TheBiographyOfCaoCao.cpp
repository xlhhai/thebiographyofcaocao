#include <stdint.h>
#include <iostream>
#include "Game.h"

const int FPS = 60;
const int DELAY_TIME = 1000.0f / FPS;

int main(int argc, char **argv){
    Uint32 frameStart, frameTime;

    std::cout << "game init attempt..." << std::endl;

    if(Game::Instance()->init("三国志 曹操传", 100, 100, 640, 440, false)){
        std::cout << "game init success!" << std::endl;

        while(TheGame::Instance()->running()){
            frameStart = SDL_GetTicks();

            TheGame::Instance()->handleEvents();
            TheGame::Instance()->update();
            TheGame::Instance()->render();

            frameTime = SDL_GetTicks() - frameStart;

            if(frameTime < DELAY_TIME){
                SDL_Delay((int)(DELAY_TIME - frameTime));
            }
        }
    } else {
        std::cout << "game init failure - " << SDL_GetError() << std::endl;
        return false;
    }

    std::cout << "game closing..." << std::endl;
    TheGame::Instance()->clean();

    return true;
}


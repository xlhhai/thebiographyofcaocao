#ifndef __LEVELL_H__
#define __LEVELL_H__

#include <iostream>
#include <vector>
#include "GameObject.h"
/*
#include "Layer.h"
#include "LevelParser.h"
#include "Player.h"
#include "CollisionManager.h"
*/

/*
class TileLayer;

struct Tileset{
    int firstGridID;
    int tileWidth;
    int tileHeight;
    int spacing;
    int margin;
    int width;
    int height;
    int numColumns;
    std::string name;
};
*/

class Level{
    public:

        ~Level();

        void update();
        void render();

        const std::string getLevelID() { return s_levelID; }
        void setLevelID( std::string id ){ s_levelID = id; }

        /*
           std::vector<Tileset>* getTilesets() { return &m_tilesets; }
           std::vector<Layer*>* getLayers() { return &m_layers; }    
           std::vector<TileLayer*>* getCollisionLayers() { return &m_collisionLayers; }
           const std::vector<TileLayer*>& getCollidableLayers() { return m_collisionLayers; }
           Player* getPlayer() { return m_pPlayer; }
           void setPlayer(Player* pPlayer) { m_pPlayer = pPlayer; }
         */

    private:

        friend class LevelParser;

        Level():
            m_loadingComplete(false), 
            m_exiting(false)
        {}

        std::string s_levelID;

        /*
           Player* m_pPlayer;
           std::vector<Layer*> m_layers;
           std::vector<Tileset> m_tilesets;
           std::vector<TileLayer*> m_collisionLayers;
         */

        bool m_loadingComplete;
        bool m_exiting;

        std::vector<GameObject*> m_gameObjects;
        std::vector<std::string> m_textureIDList;
};

#endif /* defined(__LEVELL_H__) */

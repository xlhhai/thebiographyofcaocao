#include "StateParser.h"
#include "TextureManager.h"
#include "Game.h"
#include "GameObjectFactory.h"

using namespace std;

bool StateParser::parseState(
        const char *stateFile, 
        string stateID, 
        vector<GameObject *> *pObjects, 
        std::vector<std::string> *pTextureIDs,
        std::vector<std::string> *pSoundIDs
        )
{
    TiXmlDocument xmlDoc;
    
    if(!xmlDoc.LoadFile(stateFile)){
        cerr << xmlDoc.ErrorDesc() << endl;
        return false;
    }
    
    TiXmlElement* pRoot = xmlDoc.RootElement();
    
    TiXmlElement* pStateRoot = 0;
    for(
            TiXmlElement* e = pRoot->FirstChildElement(); 
            e != NULL; 
            e = e->NextSiblingElement()
            ){
        if(e->Value() == stateID){
            pStateRoot = e;
            break;
        }
    }
    
    TiXmlElement* pTextureRoot = 0;
    for(
            TiXmlElement* e = pStateRoot->FirstChildElement(); 
            e != NULL; 
            e = e->NextSiblingElement()
            ){
        if(e->Value() == string("TEXTURES")){
            pTextureRoot = e;
            break;
        }
    }
    parseTextures(pTextureRoot, pTextureIDs);
    
    TiXmlElement* pObjectRoot = 0;
    for(
            TiXmlElement* e = pStateRoot->FirstChildElement(); 
            e != NULL; 
            e = e->NextSiblingElement()
            ){
        if(e->Value() == string("OBJECTS")){
            pObjectRoot = e;
            break;
        }
    }
    parseObjects(pObjectRoot, pObjects);
    
    return true;
}

void StateParser::parseTextures(
        TiXmlElement* pStateRoot, 
        std::vector<std::string> *pTextureIDs
        )
{
    for(
            TiXmlElement* e = pStateRoot->FirstChildElement(); 
            e != NULL; 
            e = e->NextSiblingElement()
            )
    {
        string filenameAttribute = e->Attribute("filename");
        string idAttribute = e->Attribute("ID");
        
        pTextureIDs->push_back(idAttribute);
        
        TheTextureManager::Instance()->load(
                filenameAttribute, 
                idAttribute, 
                TheGame::Instance()->getRenderer()
                );
    }
}

void StateParser::parseObjects(
        TiXmlElement *pStateRoot, 
        std::vector<GameObject *> *pObjects
        )
{
    for(
            TiXmlElement* e = pStateRoot->FirstChildElement(); 
            e != NULL; 
            e = e->NextSiblingElement()
            )
    {
        int x, y, width, height, numFrames, callbackID, animSpeed;
        string textureID;
        
        e->Attribute("x", &x);
        e->Attribute("y", &y);
        e->Attribute("width",&width);
        e->Attribute("height", &height);
        e->Attribute("numFrames", &numFrames);
        e->Attribute("callbackID", &callbackID);
        e->Attribute("animSpeed", &animSpeed);
        
        textureID = e->Attribute("textureID");

        GameObject* pGameObject = 
            TheGameObjectFactory::Instance()->create(
                e->Attribute("type")
                );

        pGameObject->load(
                std::unique_ptr<LoaderParams>(
                    new LoaderParams(
                        x, 
                        y, 
                        width, 
                        height, 
                        textureID, 
                        numFrames, 
                        callbackID, 
                        animSpeed
                        )
                    )
                );

        pObjects->push_back(pGameObject);
    }
}

void StateParser::parseSounds(
        TiXmlElement *pStateRoot, 
        std::vector<std::string> *pSoundIDs
        )
{
}

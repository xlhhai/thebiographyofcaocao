//
//  MenuState.cpp
//  SDL Game Programming Book
//
//  Created by shaun mitchell on 09/02/2013.
//  Copyright (c) 2013 shaun mitchell. All rights reserved.
//
#include <iostream>
#include "Game.h"
#include "MainMenuState.h"
#include "TextureManager.h"
#include "PlayState.h"
#include "InputHandler.h"
#include "StateParser.h"
#include "SoundManager.h"
#include "MenuButton.h"
#include "ExitState.h"
#include <unistd.h>
#include <assert.h>

const std::string MainMenuState::s_menuID = "MENU";

/* Callbacks */
void MainMenuState::s_menuToPlay(){
    TheSoundManager::Instance()->playSound("ButtonSound", 0);
    SDL_Delay(200);
    TheGame::Instance()->getStateMachine()->changeState(new PlayState());
}

void MainMenuState::s_readFromFile(){
}

void MainMenuState::s_exitFromMenu(){
    TheSoundManager::Instance()->playSound("ButtonSound", 0);
    SDL_Delay(200);
    TheGame::Instance()->getStateMachine()->changeState(new ExitState());
}
/* end callbacks */

void MainMenuState::update(){
    if(TheInputHandler::Instance()->isKeyDown(SDL_SCANCODE_SPACE)){
        s_menuToPlay();
    }

    if(!m_gameObjects.empty()){
        for(int i = 0; i < m_gameObjects.size(); i++){
            if(m_gameObjects[i] != 0){
                m_gameObjects[i]->update();
            }
        }
    }
}

void MainMenuState::render(){
    if(m_loadingComplete && !m_gameObjects.empty()){
        for(int i = 0; i < m_gameObjects.size(); i++){
            m_gameObjects[i]->draw();
        }
    }
}

bool MainMenuState::onEnter( void ){
    std::cout << "Entering MenuState\n";
    std::cout << "\tLoading sounds ...";
    TheSoundManager::Instance()->load(
            "SoundTrk/18-AudioTrack 18.mp3", 
            "MenuBackgroundMusic", 
            SOUND_MUSIC
            );
    TheSoundManager::Instance()->load(
            "SoundChunk/Se00.wav", 
            "ButtonSound", 
            SOUND_SFX
            );
    std::cout << "done" << std::endl;

    std::cout << "\tReading configuration file ...";
    StateParser stateParser;
    stateParser.parseState(
            "assets/attack.xml", 
            s_menuID, 
            &m_gameObjects, 
            &m_textureIDList,
            &m_soundIDList
            );
    std::cout << "done" << std::endl;
    
    std::cout << "\tSet callback functions ...";
    m_callbacks.push_back(0);
    m_callbacks.push_back(s_menuToPlay);
    m_callbacks.push_back(s_exitFromMenu);
    setCallbacks(m_callbacks);
    std::cout << "done" << std::endl;
    
    TheSoundManager::Instance()->playMusic("MenuBackgroundMusic", -1);
    m_loadingComplete = true;
    std::cout << "Main menu state init done!\n" << std::endl;

    return true;
}

bool MainMenuState::onExit(){
    m_exiting = true;
    
    std::cout << "Exiting MenuState" << std::endl;
    std::cout << "\tDeleting objects ...";
    if(m_loadingComplete && !m_gameObjects.empty()){
        m_gameObjects.back()->clean();
        m_gameObjects.pop_back();
    }
    m_gameObjects.clear();
    std::cout << "done" << std::endl;

    
    std::cout << "\tClearing the texture manager ...";
    for(int i = 0; i < m_textureIDList.size(); i++){
        TheTextureManager::Instance()->clearFromTextureMap(m_textureIDList[i]);
    }
    std::cout << "done" << std::endl;
    
    std::cout << "\tReset input handler ...";
    TheInputHandler::Instance()->reset();
    std::cout << "done" << std::endl;
    std::cout << "Main menu state exit!\n" << std::endl;
    
    return true;
}

void MainMenuState::setCallbacks(const std::vector<Callback>& callbacks){
    if(!m_gameObjects.empty()){
        for(int i = 0; i < m_gameObjects.size(); i++){
            if(dynamic_cast<MenuButton*>(m_gameObjects[i])){
                MenuButton* pButton = dynamic_cast<MenuButton*>(m_gameObjects[i]);
                pButton->setCallback(callbacks[pButton->getCallbackID()]);
            }
        }
    }
}

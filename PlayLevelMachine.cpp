#include "PlayLevelMachine.h"
#include <iostream>

void PlayLevelMachine::clean(){
    if( !m_playLevels.empty() ){
        m_playLevels.back()->onExit();
        delete m_playLevels.back();
        m_playLevels.clear();
    }
}

void PlayLevelMachine::update(){
    if( !m_playLevels.empty() ){
        m_playLevels.back()->update();
    }
}

void PlayLevelMachine::render(){
    if( !m_playLevels.empty() ){
        m_playLevels.back()->render();
    }
}

void PlayLevelMachine::pushLevel( Level *pLevel ){
    m_playLevels.push_back( pLevel );
    m_playLevels.back()->onEnter();
}

void PlayLevelMachine::popLevel( Level *pLevel ){
    if( !m_playLevels.empty() ){
        m_playLevels.back()->onExit();
        m_playLevels.pop_back();
    }
}

void PlayLevelMachine::changeLevel( Level *pLevel ){
    if( !m_playLevels.empty() ){
        if( m_playLevels.back()->getLevelID() == pLevel->getLevelID() ){
            return;
        }

        m_playLevels.back()->onExit();
        m_playLevels.pop_back();
    }

    pLevel->onEnter();
    m_playLevels.push_back( pLevel );
}

#ifndef __MAINMENUSTATE_H__
#define __MAINMENUSTATE_H__

#include <vector>
#include "MenuState.h"
#include "GameObject.h"

class MainMenuState : public MenuState{
    public:

        virtual ~MainMenuState() {}

        virtual void update();
        virtual void render();

        virtual bool onEnter(); 
        virtual bool onExit(); 

        virtual std::string getStateID() const { return s_menuID; }

    private:

        virtual void setCallbacks(const std::vector<Callback>& callbacks);

        /*  Call back functions  */
        static void s_menuToPlay();
        static void s_readFromFile();
        static void s_exitFromMenu();

        static const std::string s_menuID;
        std::vector<GameObject*> m_gameObjects;
};

#endif /* defined(__MAINMENUSTATE_H__) */

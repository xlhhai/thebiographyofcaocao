#include "Game.h"
#include "TextureManager.h"
#include "InputHandler.h"
#include "MainMenuState.h"
#include "MenuButton.h"
#include "GameObjectFactory.h"
#include "Background.h"
#include "SoundManager.h"

Game* Game::s_pInstance = 0;

Game::Game() :
m_pWindow(0),
m_pRenderer(0),
m_bRunning(false)
{
    m_currentLevel = 1;
    m_levelFiles.push_back("Data/level1.xml");
}

Game::~Game(){
}

bool Game::init(
        const char* title, 
        int xpos, 
        int ypos, 
        int width, 
        int height, 
        bool fullscreen
        )
{ 
    int flags = 0;
    
    m_gameWidth = width;
    m_gameHeight = height;
    
    if(fullscreen){
        flags = SDL_WINDOW_FULLSCREEN;
    }
    
    if(SDL_Init(SDL_INIT_EVERYTHING) == 0){
        std::cout << "SDL init success" << std::endl;

        m_pWindow = SDL_CreateWindow(title, xpos, ypos, width, height, flags);
        
        if(m_pWindow != 0){
            std::cout << "window creation success" << std::endl;
            m_pRenderer = SDL_CreateRenderer(
                    m_pWindow, 
                    -1, 
                    SDL_RENDERER_ACCELERATED
                    );
            
            if(m_pRenderer != 0){
                std::cout << "renderer creation success" << std::endl;
                SDL_SetRenderDrawColor(m_pRenderer, 0,0,0,255);
            }else{
                std::cout << "renderer init fail" << std::endl;
                return false;
            }
        } else{
            std::cout << "window init fails" << std::endl;
            return false;
        }
    } else{
        std::cout << "SDL init fail" << std::endl;
        return false;
    }
    
    TheGameObjectFactory::Instance()->registerType(
            "Background", 
            new BackgroundCreator()
            );
    TheGameObjectFactory::Instance()->registerType(
            "MenuButton", 
            new MenuButtonCreator()
            );

    m_pGameStateMachine = new GameStateMachine();
    m_pGameStateMachine->changeState(new MainMenuState());

    m_bRunning = true;

    return true;
}

void Game::render(){
    SDL_RenderClear(m_pRenderer);
    m_pGameStateMachine->render();
    SDL_RenderPresent(m_pRenderer);
}

void Game::update(){
    m_pGameStateMachine->update();
}

void Game::handleEvents(){
    TheInputHandler::Instance()->update();
}

void Game::clean(){
}

#include <string>
#include "LevelParser.h"
#include "TextureManager.h"
#include "Game.h"
//#include "ObjectLayer.h"
#include "GameObjectFactory.h"
#include "Level.h"

Level* LevelParser::parseLevel(const char *levelFile){
    TiXmlDocument levelDocument;

    std::cout << "Hello!" << std::endl;
    
    //if( !levelDocument.LoadFile(levelFile) ){
    if( !levelDocument.LoadFile("Data/level1.xml") ){
        std::cerr << levelDocument.ErrorDesc() << std::endl;
        return NULL;
    }

    pLevel = new Level();
    
    TiXmlElement* pRoot = levelDocument.RootElement();
    
    std::cout << "Loading level " << pRoot->Attribute("id") << ": "<< std::endl; 
    pLevel->setLevelID( pRoot->Attribute("id") );

    TiXmlElement* pPlot = 0;
    TiXmlElement* pCombat = 0;

    for(
            TiXmlElement* e = pRoot->FirstChildElement(); 
            e != NULL; 
            e = e->NextSiblingElement()
       )
    {
        std::cout << e->Value() << std::endl; 

        if(e->Value() == std::string("plot")){
            pPlot = e;
        } else if ( e->Value() == std::string("combat") ){
            pCombat = e;
        }
    }

    parsePlot( pPlot );
    //parseCombat( pCombat );

    std::cout << "Loading level " << pRoot->Attribute("id") << " done!"<< std::endl; 

    return pLevel;
}


void LevelParser::parsePlot( TiXmlElement* pPlotRoot ){
    std::cout << "Loading plot ..." << std::endl; 

    TiXmlElement* pTextures = 0;
    TiXmlElement* pObjects = 0;
    
    for(TiXmlElement* e = pPlotRoot->FirstChildElement(); 
            e != NULL; 
            e = e->NextSiblingElement()
            )
    {
        std::cout << e->Value() << std::endl; 

        if(e->Value() == std::string("textures")){
            pTextures = e;
        } else if( e->Value() == std::string( "objects" )){
            pObjects = e;
        }
    }

    parseTextures( pTextures );
    parseObjects( pObjects );

    std::cout << "Loading plot done!"<< std::endl; 
}

void LevelParser::parseCombat( TiXmlElement* pCombatRoot ){
    TiXmlElement* pTextures = 0;
    TiXmlElement* pObjects = 0;
    
    for(TiXmlElement* e = pCombatRoot->FirstChildElement();
            e != NULL; 
            e = e->NextSiblingElement()
            )
    {
        if(e->Value() == std::string("textures")){
            pTextures = e;
        } else if( e->Value() == std::string( "objects" )){
            pObjects = e;
        }
    }

    parseTextures( pTextures );
    parseObjects( pObjects );
}

void LevelParser::parseTextures(TiXmlElement* pTextureRoot){
    for(TiXmlElement* e = pTextureRoot->FirstChildElement(); 
            e != NULL; 
            e = e->NextSiblingElement()
            )
    {
        std::cout << e->Value() << std::endl; 

        if( e->Value() == std::string("texture") ){
            std::cout << "adding texture " 
                << e->Attribute("filename") 
                << " with ID " 
                << e->Attribute("id") 
                << std::endl;

            TheTextureManager::Instance()->load(
                    e->Attribute("filename"), 
                    e->Attribute("id"), 
                    TheGame::Instance()->getRenderer()
                    );
        }
    }
}

void LevelParser::parseObjects(TiXmlElement* pObjectRoot){
    for(TiXmlElement* e = pObjectRoot->FirstChildElement(); 
            e != NULL; 
            e = e->NextSiblingElement()
            )
    {
        int x, y, width, height, numFrames, callbackID, animSpeed;
        std::string textureID;

        if( e->Value() == std::string( "object" ) ){
                std::cout << "adding object" 
                          << e->Attribute("type")
                          << " with ID "
                          << e->Attribute("textureID")
                          << std::endl;
        }

        e->Attribute("x", &x);
        e->Attribute("y", &y);
        e->Attribute("width",&width);
        e->Attribute("height", &height);
        e->Attribute("numFrames", &numFrames);
        e->Attribute("callbackID", &callbackID);
        e->Attribute("animSpeed", &animSpeed);
        textureID = e->Attribute("textureID");

        GameObject* pGameObject = 
            TheGameObjectFactory::Instance()->create(
                e->Attribute("type")
                );

        pGameObject->load(
                std::unique_ptr<LoaderParams>(
                    new LoaderParams(
                        x, 
                        y, 
                        width, 
                        height, 
                        textureID, 
                        numFrames, 
                        callbackID, 
                        animSpeed
                        )
                    )
                );

        pLevel->m_gameObjects.push_back(pGameObject);
    }
}

#if 0
void LevelParser::parseObjectLayer(
        TiXmlElement* pObjectElement, 
        std::vector<Layer*> *pLayers, 
        Level* pLevel
        )
{
    ObjectLayer* pObjectLayer = new ObjectLayer();
    
    for(TiXmlElement* e = pObjectElement->FirstChildElement(); 
            e != NULL; 
            e = e->NextSiblingElement()
            )
    {
        if(e->Value() == std::string("object")){
            int x, y, width, height, numFrames, callbackID = 0, animSpeed = 0;
            std::string textureID;
            std::string type;
            
            e->Attribute("x", &x);
            e->Attribute("y", &y);
            
            type = e->Attribute("type");
            GameObject* pGameObject = TheGameObjectFactory::Instance()->create(type);
            
            for(TiXmlElement* properties = e->FirstChildElement(); 
                    properties != NULL; 
                    properties = properties->NextSiblingElement()
                    )
            {
                if(properties->Value() == std::string("properties")){
                    for(TiXmlElement* property = properties->FirstChildElement(); 
                            property != NULL; 
                            property = property->NextSiblingElement()
                            )
                    {
                        if(property->Value() == std::string("property")){
                            if(property->Attribute("name") == std::string("numFrames")){
                                property->Attribute("value", &numFrames);
                            } else if(property->Attribute("name") == std::string("textureHeight")){
                                property->Attribute("value", &height);
                            }else if(property->Attribute("name") == std::string("textureID")){
                                textureID = property->Attribute("value");
                            }else if(property->Attribute("name") == std::string("textureWidth")){
                                property->Attribute("value", &width);
                            }else if(property->Attribute("name") == std::string("callbackID")){
                                property->Attribute("value", &callbackID);
                            }else if(e->Attribute("name") == std::string("animSpeed")){
                                property->Attribute("value", &animSpeed);
                            }
                        }
                    }
                }
            }
            pGameObject->load(
                    std::unique_ptr<LoaderParams>(
                        new LoaderParams(
                            x, 
                            y, 
                            width, 
                            height, 
                            textureID, 
                            numFrames,
                            callbackID, 
                            animSpeed
                            )
                        )
                    );
            
            /* 
            if(type == "Player"){
                pLevel->setPlayer(dynamic_cast<Player*>(pGameObject));
            } */
            
            pObjectLayer->getGameObjects()->push_back(pGameObject);
        }
    }
    
    pLayers->push_back(pObjectLayer);
}
#endif
